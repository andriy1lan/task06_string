package strings;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class FirstStringTask {

    String text;

    public void setText(String text) {
        this.text=text;
    }

    public void findWithConcurrentWords() {
        Pattern p = Pattern.compile("[^.]*\\b(\\w+)\\b[^.]*\\b(\\1)\\b[^.]*\\.?", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        int i = 0;
        while (m.find()) { i++ ;
            System.out.println("Found Sentence: " + m.group());
            System.out.println("Recurrent words: " + m.group(1) + " " + m.group(2));
        }
        System.out.println("Number of found sentences: " + i);
    }

    public static void main (String[] args) {

        FirstStringTask sp = new FirstStringTask();
        String txt="The Java programming language is the most popular amongst programming languages, so java programmers are strongly required at job market."
                + "The main advantages of java language are its object oriented features and the cross-platformity. Except these two, additional advantage is extended library"
                + " environment developed about two last decades";
        sp.setText(txt);
        sp.findWithConcurrentWords();

    }

}
