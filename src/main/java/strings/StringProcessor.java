package strings;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.net.URISyntaxException;
import java.util.stream.*;

public class StringProcessor {

    static String text;

    public static void readFromFile() throws URISyntaxException, IOException {
        //text = new String(Files.readAllBytes(Paths.get("file.txt")));
        //file.txt is located in src/main/resources
        text = new String(Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("file.txt").toURI())));
    }


    public static void findWordsfromFirstinOthers(String text) {
        Sentence.createSentences(text);
        List<String> nowords = new ArrayList<>();
        Sentence sfirst = new Sentence();
        sfirst.createWords(Sentence.sentences[0]);
        Arrays.asList(sfirst.words).stream().distinct().forEach((w) -> {
            boolean notfound = false;
            int i = 0;
            for (String sen : Sentence.sentences) {
                i++;
                if (i == 1) continue;
                Sentence current = new Sentence(sen);
                current.createWords(sen);
                notfound = Arrays.asList(current.words).stream().anyMatch(cw -> cw.equals(w));
                if (notfound == true) break;
            }
            if (notfound == false) nowords.add(w);
        });
        System.out.println("First sentence: ");
        System.out.println(Sentence.sentences[0]);
        System.out.println("No found words in another sentences: ");
        for (String noword : nowords) {
            System.out.print(noword + " ");
        }
        System.out.println();
    }


    public static void sortSentencesByWordsCount(String text) {
        List<Sentence> sentens = new ArrayList<>();
        Sentence.createSentences(text);
        for (String sen : Sentence.sentences) {
            Sentence s = new Sentence(sen);
            s.createWords(sen);
            sentens.add(s);
        }
        sentens.sort(new Comparator<Sentence>() {
            @Override
            public int compare(Sentence o1, Sentence o2) {
                return o1.wordssize - o2.wordssize;
            }
        });
        System.out.println("Sorted list of sentences in ascending order accordingly to their words count: ");
        for (Sentence sen : sentens) {
            System.out.println(sen.sentencetext + " " + sen.wordssize);
        }
        System.out.println();
    }

    public static void printDistinctsInQuestionSentences(int n) {
        String questions = "Hello Student! Did you learn the Java Language enough? "
                +"Do you use books, practice tasks, seminars, online resources? "
                +"The best way is to use appropriate courses to study in team environment.";
        System.out.println("The short text with question marks sentences :");
        System.out.println(questions);
        System.out.println("Found sentences and theirs words of "+n+" length.");
        Pattern pattern = Pattern.compile("([A-Z][^.!?]+\\?+\\s*?)");
        Matcher matcher = pattern.matcher(questions);
        while(matcher.find()) {
            System.out.println(matcher.group(1));
            Pattern pattern1 = Pattern.compile("\\b\\w{"+n+"}\\b");
            Matcher matcher1 = pattern1.matcher(matcher.group(1));
            while(matcher1.find()) {
                System.out.println(matcher1.group(0)); }
        }
        System.out.println();
    }

    public static void printWordsAlphabetically(String text) {
        System.out.println("Alphabetical list of distinct words in the text:");
        Sentence.createSentences(text);
        int counts = Sentence.sentences.length;
        String[][] wordsdata = new String[counts][];
        int i = 0;
        for (String sen : Sentence.sentences) {
            Sentence s = new Sentence();
            s.createWords(sen);
            wordsdata[i] = s.words;
            i++ ;
        }
        Stream<String[]> temp = Arrays.stream(wordsdata);
        Stream<String> stringStream = temp.flatMap(x -> Arrays.stream(x));
        stringStream.filter(w->w.matches("^[A-Za-z].*")).map(w -> Character.toUpperCase(w.charAt(0)) + w.substring(1)).sorted().distinct().forEach(System.out::println);
        System.out.println();
    }


    public static void replaceFirstWithStVowelforLongest(String text) {
        Sentence.createSentences(text);
        int i = 1;
        for (String sen : Sentence.sentences) {
            System.out.println(i + "." + sen);
            String patternString="\\b[AEIOUaeiou]{1}[a-z0-9.]+\\b";
            Pattern pattern = Pattern.compile(patternString);
            Matcher matcher = pattern.matcher(sen);
            String vowel = null;
            while(matcher.find()) {
                vowel = matcher.group(0);
                break;
            }
            if (vowel != null) System.out.println("starting vowel word: " +vowel);
            Sentence s = new Sentence();
            s.createWords(sen);
            String longest = Arrays.asList(s.words).stream().max(Comparator.comparingInt
                    (String::length)).orElse(null);
            if (longest != null) System.out.println("longest word: " + longest);
            if (vowel != null) sen=sen.replace(vowel,longest);
            else System.out.println("No starting vowel word");
            System.out.println("new: " + sen);
            i++ ;
        }
        System.out.println();
    }

    public static void sortWordsByWovelPercent(String text) {
        Sentence.createSentences(text);
        int counts = Sentence.sentences.length;
        String[][] wordsdata = new String[counts][];
        int i=0;
        for (String sen : Sentence.sentences) {
            Sentence s = new Sentence();
            s.createWords(sen);
            wordsdata[i] = s.words;
            i++;
        }
        Comparator<Word> comparatorByPercents = Comparator.comparing(Word::getVowelPercentage).thenComparing(Word::getWord);
        Stream<String[]> temp = Arrays.stream(wordsdata);
        Stream<String> stringStream = temp.flatMap(x -> Arrays.stream(x));
        stringStream.filter(w->w.matches("^[A-Za-z].*")).map(w -> Character.toUpperCase(w.charAt(0))+w.substring(1)).distinct().map(w->new Word(w)).
                sorted(comparatorByPercents).map(w->w.getWord() + " " + new Double(w.getVowelPercentage()).toString()).forEach(System.out::println);
        System.out.println();
    }

    public static void sortVowelWordsByFirstConsonant(String text) {
        Sentence.createSentences(text);
        int counts = Sentence.sentences.length;
        String[][] wordsdata = new String[counts][];
        int i = 0;
        for (String sen : Sentence.sentences) {
            Sentence s = new Sentence();
            s.createWords(sen);
            wordsdata[i] = s.words;
            i++;
        }
        Comparator<Word> comparatorByConsonant = Comparator.comparing(Word::getFirstConsonant).thenComparing(Word::getWord);
        Stream<String[]> temp = Arrays.stream(wordsdata);
        Stream<String> stringStream = temp.flatMap(x -> Arrays.stream(x));
        stringStream.filter(w->w.matches("^[A-Za-z].*")).map(w -> Character.toUpperCase(w.charAt(0))+w.substring(1)).filter(w->w.matches("^[AEIOU].*")).distinct().map(w->new Word(w)).
                sorted(comparatorByConsonant).map(w->w.getWord()+ " " + w.getFirstConsonant()).forEach(System.out::println);
        System.out.println();
    }




    public static void main(String[] args) throws Exception {
        readFromFile();
        sortSentencesByWordsCount(text);
        findWordsfromFirstinOthers(text);
        replaceFirstWithStVowelforLongest(text);
        printDistinctsInQuestionSentences(5);
        printWordsAlphabetically(text);
        sortWordsByWovelPercent(text);
        sortVowelWordsByFirstConsonant(text);
    }
}