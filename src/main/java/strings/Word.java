package strings;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

class Word {

    String word;
    int size;

    public Word() {}

    public Word (String word) {
        this.word = word;
        this.size = word.length();
    }

    public String getWord() {
        return word;
    }

    public String getFirstConsonant() {
        Pattern pattern = Pattern.compile("\\b[AEIOUaeiou]+([^AEIOUaeiou])+?\\w*\\b");
        Matcher matcher = pattern.matcher(word);
        String fconsonant = null;
        while(matcher.find()) {
            fconsonant = matcher.group(1);
        }

        if (fconsonant!=null) return fconsonant;
        else return " no consonant";

    }


    public double getVowelPercentage() {
        long wovelcount = word.chars()
                .mapToObj(c -> (char) c).filter(c-> c.equals('a') || c.equals('A') || c.equals('e') || c.equals('E') || c.equals('i') || c.equals('I') || c.equals('o') || c.equals('O') || c.equals('u') || c.equals('U')).count();
        double percent=((double)wovelcount)/size;
        return percent;
        //return Double.parseDouble(new DecimalFormat("##.####").format(percent));
    }
}