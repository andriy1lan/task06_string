package strings;

import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class Sentence {
    static String [] sentences;
    static int size;
    String [] words;
    int wordssize;
    String sentencetext;

    public Sentence () {
    }

    public Sentence (String sentencetext) {
        this.sentencetext = sentencetext;
    }

    public static void createSentences(String text) {
        sentences = text.split("[.!?]+\\s+(?=[A-Z0-9])");
        String last = sentences[sentences.length - 1];
        last = last.substring(0,last.length() - 1);
        sentences[sentences.length-1] = last;
        size=sentences.length;
    }

    public void createWords(String sentence) {
        sentence = sentence.replaceAll("[.!?,]+(?=\\s)","");
        words = sentence.split("[\\s+]");
        wordssize = words.length;
    }

}
